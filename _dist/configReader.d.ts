import { ConfigIter } from './type';
declare function nestedObjectAssigner(variable: ConfigIter, toCheckWith: ConfigIter): any;
declare const config: any;
export { config, nestedObjectAssigner };

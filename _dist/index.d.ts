export declare function emerg<T>(val: T): void;
export declare function alert<T>(val: T): void;
export declare function crit<T>(val: T): void;
export declare function error<T>(val: T): void;
export declare function warning<T>(val: T): void;
export declare function notice<T>(val: T): void;
export declare function info<T>(val: T): void;
export declare function debug<T>(val: T): void;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const moment_1 = __importDefault(require("moment"));
const configReader_1 = require("./configReader");
const util_1 = __importDefault(require("util"));
function clock() {
    return chalk_1.default `[{${'gray.bold'} ${moment_1.default().format(configReader_1.config.clock.format)}}]`;
}
function baseLogger(color, val) {
    console.log(clock()
        + chalk_1.default ` {${configReader_1.config.logger[color].color} ${val}.}`);
}
function emerg(val) { baseLogger('emerg', val); }
exports.emerg = emerg;
function alert(val) { baseLogger('alert', val); }
exports.alert = alert;
function crit(val) { baseLogger('crit', val); }
exports.crit = crit;
function error(val) { baseLogger('error', val); }
exports.error = error;
function warning(val) { baseLogger('warning', val); }
exports.warning = warning;
function notice(val) { baseLogger('notice', val); }
exports.notice = notice;
function info(val) { baseLogger('info', val); }
exports.info = info;
function debug(val) { baseLogger('debug', val); }
exports.debug = debug;

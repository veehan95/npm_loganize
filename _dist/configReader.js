"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const defaults_1 = __importDefault(require("./defaults"));
const configDir = `${process.cwd()}/loganize.config.json`;
function nestedObjectAssigner(variable, toCheckWith) {
    if (typeof variable === 'object') {
        const keyList = Object.keys(variable);
        return keyList.map((key) => {
            if (nestedObjectAssigner(variable[key], toCheckWith[key])) {
                Object.assign(toCheckWith[key], variable[key]);
                return toCheckWith[key];
            }
        });
    }
    return true;
}
exports.nestedObjectAssigner = nestedObjectAssigner;
const config = fs_1.default.existsSync(configDir)
    ? nestedObjectAssigner(require(configDir), defaults_1.default)
    : defaults_1.default;
exports.config = config;

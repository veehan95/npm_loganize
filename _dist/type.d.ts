declare type Clock = {
    display?: boolean;
    format?: string;
};
declare type OutDir = {
    baseDir?: string;
};
declare type LogProp = {
    color?: string;
    path?: string;
};
declare type LogType = "emerg" | "alert" | "crit" | "error" | "warning" | "notice" | "info" | "debug";
declare type FullConfig = {
    clock?: Clock;
    ourDir?: OutDir;
    [s: string]: any;
};
declare type ConfigIter = {
    [s: string]: any;
};
export { Clock, ConfigIter, FullConfig, LogProp, LogType, OutDir };

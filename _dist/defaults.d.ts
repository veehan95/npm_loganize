declare const _default: {
    clock: {
        display: boolean;
        format: string;
    };
    ourDir: {
        baseDir: string;
    };
    logger: {
        emerg: {
            color: string;
            path: string;
        };
        alert: {
            color: string;
            path: string;
        };
        crit: {
            color: string;
            path: string;
        };
        error: {
            color: string;
            path: string;
        };
        warning: {
            color: string;
            path: string;
        };
        notice: {
            color: string;
            path: string;
        };
        info: {
            color: string;
            path: string;
        };
        debug: {
            color: string;
            path: string;
        };
    };
};
export default _default;

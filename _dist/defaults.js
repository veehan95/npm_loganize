"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    clock: {
        display: true,
        format: "YY/MM/DD HH:MM:SS"
    },
    ourDir: {
        baseDir: "./_logs"
    },
    logger: {
        emerg: {
            color: "black.bgRed",
            path: "/emerg"
        },
        alert: {
            color: "yellowBright",
            path: "/alert"
        },
        crit: {
            color: "black.bgRedBright",
            path: "/crit"
        },
        error: {
            color: "redBright",
            path: "/error"
        },
        warning: {
            color: "black.bgYellowBright",
            path: "/warning"
        },
        notice: {
            color: "greenBright",
            path: "/notice"
        },
        info: {
            color: "black.bgGreenBright",
            path: "/info"
        },
        debug: {
            color: "white",
            path: "/debug"
        }
    }
};

type Clock  = {
  display?: boolean,
  format?: string
}

type OutDir = {
  baseDir?: string
}

type LogProp = {
  color?: string,
  path?: string
}

type LogType = "emerg"|"alert"|"crit"|"error"|"warning"|"notice"|"info"|"debug"

type FullConfig = {
  clock?: Clock,
  ourDir?: OutDir,
  [s: string]: any
  // logger?: { [key in LogType]: LogProp }
}

// type ConfigIter = Clock|FullConfig|LogProp|LogType|OutDir|string|boolean|{}
type ConfigIter = {[s: string]: any}

export {
  Clock,
  ConfigIter,
  FullConfig,
  LogProp,
  LogType,
  OutDir
}
import chalk from 'chalk'
import moment from 'moment'
import { config } from './configReader'
import util from 'util'

function clock (): string {
  return chalk`[{${'gray.bold'} ${moment().format(config.clock.format)}}]`
}


function baseLogger (color: string, val: any): void {
  console.log(clock()
    + chalk` {${config.logger[color].color} ${val}.}`)
}

export function emerg<T> (val: T) { baseLogger('emerg', val) }
export function alert<T> (val: T) { baseLogger('alert', val) }
export function crit<T> (val: T) { baseLogger('crit', val) }
export function error<T> (val: T) { baseLogger('error', val) }
export function warning<T> (val: T) { baseLogger('warning', val) }
export function notice<T> (val: T) { baseLogger('notice', val) }
export function info<T> (val: T) { baseLogger('info', val) }
export function debug<T> (val: T) { baseLogger('debug', val) }

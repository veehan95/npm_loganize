import fs from 'fs'
import default_config from './defaults'
import { ConfigIter } from './type'

const configDir: string = `${process.cwd()}/loganize.config.json`

function nestedObjectAssigner (variable: ConfigIter, toCheckWith: ConfigIter): any {
  if (typeof variable === 'object') {
    const keyList: string[] = Object.keys(variable)

    return keyList.map((key: string) => {
      if (nestedObjectAssigner(variable[key],toCheckWith[key])) {
        Object.assign(toCheckWith[key], variable[key])
        return toCheckWith[key]
      }
    })
  }

  return true
}

const config = fs.existsSync(configDir)
  ? nestedObjectAssigner(require(configDir), default_config)
  : default_config

export {
  config,
  nestedObjectAssigner
}